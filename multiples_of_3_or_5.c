int sum_mult_of_m(int m, int num) {
    // sum all multiples of m in the half-open range [1 to num)
    
    // find the largest multiple of m in the range
  
    int largest = 0;
  
    // this loop will find at least one multiple of m if num > m (because m itself is in the range)
    // if num <= m, there are no multiples, the loop does not execute, and largest remains 0
    for (largest = num-1; largest >= m; largest--) {
        if (largest % m == 0) {
          break; // largest holds the largest value divisible by m
        }
    }
    
    int num_multiples = largest / m; // may be 0 if no largest multiple found
    
    int sum = (num_multiples / 2) * (m + largest);
    if (num_multiples % 2 != 0) {
      sum += m + (largest - m)/2; // if odd number of multiples, add the middle one in in an overflow-safe way
    }
  
    return sum;
}

int solution(int number) {
		// code here
    int sum = 0;
    sum += sum_mult_of_m(3, number);
    sum += sum_mult_of_m(5, number);  
    // subtract out the sum of the multiples of 15 one time, each
    sum -= sum_mult_of_m(15, number);
  
    return sum;
}